# RetroPie automated script for x86 (UNOFFICIAL) (Ubuntu Server 20.04+)

# Setup

1. Might have to install `git`; if so `sudo apt install git`

2. Clone repo `git clone https://gitlab.com/krazynez/retropie_setup_x86.git`

3. `cd retropie_setup_x86`

4. `chmod +x retropie_setup_x86.sh`


# Running

\* **DO NOT RUN WITH SUDO OR AS ROOT** *

1. `./retropie_setup_x86.sh`

_Caveat:_ You will have to put in your `sudo` password possibly twice. Depending on the speed of your install.

<br><br><br>

## Dev Repo

\* **NOTE: This is for development and NOT fully tested on most case scenarios, or possibly newer than master branch** *

1. `./retropie_setup_x86.sh -V|--version`

	1. Make sure you remember the version number

	2. `git checkout testing`

	3. `./retropie_setup_x86.sh -V|--version`

	4. if the testing version is newer go ahead and continue, else **STOP** and use master branch `git checkout master`


2. `./retropie_setup_x86.sh`


## Have a problem?

 - Make an [issue](https://gitlab.com/krazynez/retropie_setup_x86/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) with my repo. The Official RetroPie people are hard at work. **DO NOT BUG THEM** They do not support this script officially, even though it is based off of the original official tutorial.

 - Logs will be located in `errors.log` in the retropie\_setup\_x86 folder
