#!/usr/bin/env bash

#####################################################################
#                                                                   #
# Author: Brennen Murphy                                            #
#                                                                   #
# Date: 23 April 2021                                               #
#                                                                   #
# GitLab: http://gitlab.com/krazynez                                # 
#                                                                   #
# Project's Gitlab: https://gitlab.com/krazynez/retropie_setup_x86  #
#                                                                   #
#####################################################################


# Used for auto login, only useful if using sudo; will not work right with actual root installing
default_user=$(users | awk '{print $1}')

# Used to check if x86 machine
os_type=$(uname -m)

# List of required packages to start emultationstation with X server
PACKAGES="git gcc g++ build-essential python3-pyudev dialog unzip xmlstarlet xserver-xorg-core xserver-xorg-input-libinput xserver-xorg-input-evdev xserver-xorg-input-joystick xserver-xorg-input-kbd xserver-xorg-input-mouse xserver-xorg-input-multitouch xserver-xorg-input-synaptics xserver-xorg-input-xwiimote pulseaudio openbox xterm xinit alsa-base alsa-utils xserver-xorg-video-fbdev"

function uninstall() {

	if [[ -d "/home/$default_user/RetroPie-Setup" ]]; then
		dialog --colors --msgbox "Please use the \Z1"Uninstall"\Zn in the RetroPie setup script to remove RetroPie." 15 45
		cd /home/$default_user/RetroPie-Setup && sudo ./retropie_setup.sh
		cd -
		sudo rm -rf /home/$default_user/RetroPie-Setup
	fi

	if [[ -d "/home/$default_user/RetroPie" ]]; then
        prompt=$((dialog --colors --defaultno --yesno "Do you want to remove the RetroPie folder?\n\n\Z1\ZuThis WILL delete all your ROMS, BIOS, etc..." 10 60))
		#read -p "Do you want to remove the RetroPie folder?\nThis WILL delete all your ROMS, BIOS, etc.. (y/N): " userInput
		if [[ $? -eq 0 ]]; then
            dialog --clear
			dialog --sleep 3 --infobox "Removing RetroPie folder" 10 20
			rm -rf "/home/$default_user/RetroPie"
		else
			dialog --sleep 3 --infobox "Keeping RetroPie folder" 10 20
		fi
	fi

	# awk is used to remove `git` from the PACKAGE variable to keep, the `sed` is used to remove the first space after removing the git word.
	if [[ "$1" == "--no-ask" ]]; then
		sudo apt-get purge -y `echo $PACKAGES | awk '{$1=""; print}' | sed -e '0,/[[:space:]]/s///'`

	else

		for i in `echo $PACKAGES | awk '{$1=""; print}' | sed -e '0,/[[:space:]]/s///'`; do
			sudo apt-get purge $i
		done	

	fi
	# Remove everything that was either created or edited by RetroPie_Setup_x86 script
	rm /home/$default_user/.xinitrc && rm /home/$default_user/.bash_profile
	sudo rm -rf "/etc/systemd/system/getty@tty1.service.d/override.conf"
	sudo systemctl daemon-reload
	sudo systemctl enable "getty@tty1.service"
	sudo update-grub
	sudo sed -i "/$default_user  ALL=(ALL)   NOPASSWD:ALL/d" /etc/sudoers
	sudo systemctl daemon-reload


	return 0;
}

function err() {
	echo "An Error Occurred:"
	awk 'NR>L-4 && NR<L+4 { printf "%-5d%3s%s\n",NR,(NR==L?"ERROR >>> ":""),$0 }' L=$1 $0 2>&1 > errors.log

	if [[ "$2" == "package_install_err" ]]; then
		dialog --sleep 3 --infobox "Installed packages will be removed for sanity check." 10 20
		sudo apt-get purge -y `echo $PACKAGES | awk '{$1=""; print}' | sed -e '0,/[[:space:]]/s///'`
		exit 1;
	elif [[ "$2" == "retropie_setup_err" ]]; then
		dialog --sleep 3 --infobox "RetroPie Setup failed!!" 10 20
		exit 1;
	elif [[ "$2" == "restore_services_err" ]]; then
		dialog --sleep 3 --infobox "Error occured when trying to install custom systemctl service" 10 20
        # Need to find why this is getting triggered deleting a perfectly fine override.conf
		sudo rm "/etc/systemd/system/getty@tty1.service.d/override.conf"
		exit 1;
	elif [[ "$2" == "emulationstation_autostart_err" ]]; then
		dialog --sleep 3 --infobox "Error: failed to create EmulationStation autostart script" 10 20
		exit 1;
	fi

    dialog --clear

}

function check_dialog() {
if [[ ! -f "/usr/bin/dialog" ]]; then
    read -p "Dialog program required. Install? (y/N): " input
    if [[ "$input" =~ ^(YES|YEs|YeS|yES|yeS|yEs|yes|y|Y)$ ]]; then
        sudo apt-get update && sudo apt-get install dialog
        return 0;
    else
        printf "You need to install dialog first\n"
        exit 1;
    fi
else
    return 0;
fi

}

function get_version() {
	printf "1.35"
}


# Version MENU
if [[ "$1" == "--version" || "$1" == "-V" ]]; then
    check_dialog
    if [[ $? -eq 0 ]]; then
        dialog --colors --sleep 3 --infobox "\n\Z1Version:\Zn $(get_version)" 5 20
        clear
	    exit 0;
   fi

# Help MENU
elif [[ "$1" == "--help" || "$1" == "-h" ]]; then
    check_dialog
    if [[ $? -eq 0 ]]; then
	    dialog --scrollbar --msgbox "\nGet the version number: --version|-V\n\nAvailable branches: --list-branches|-l \n\nUse AMDGPU (non-free) drivers: --non-free\n    If NVIDIA is detected, it will auto install the Proprietary drivers.\n\nTo Uninstall: --uninstall\nUninstall without asking each package removal: --uninstall --no-ask\n\n" 20 80
        clear
	    exit 0;
    fi

# List Branches MENU    
elif [[ "$1" == "--list-branches" || "$1" == "-l" ]]; then 
    check_dialog
    if [[ $? -eq 0 ]]; then
	    git show-branch | awk '{print $2}' | grep [a-z] | sed 's/[][^~0-9]//g' | sort | uniq
	    exit 0;
    fi

# Uninstall MENUS
elif [[ "$1" == "--uninstall" && "$2" == "--no-ask" ]]; then
    check_dialog
    if [[ $? -eq 0 ]]; then
        uninstall "--no-ask"
        exit 0;
    fi
elif [[ "$1" == "--uninstall" ]]; then
    check_dialog
    if [[ $? -eq 0 ]]; then
        uninstall
        exit 0;
    fi
fi

################### PREREQUISITES ######################


# Check if script is run as root
if [[ $EUID -eq 0 ]]; then
	echo "Do not run this script as root."
	exit 1;
fi

# Check to see if dialog is installed (for Version 1.35+)
check_dialog

# Check os type and if on Ubuntu NOTE: =~ is to apply regex
if [[ ! $os_type =~ ^(x86_64|amd64)$ ]]; then
	dialog --sleep 2 --infobox "$os_type unsupported." 10 20
	exit 1;
elif [[ ! -f '/usr/bin/apt' ]]; then
	dialog --sleep 2 --infobox "Unsupported distribution make sure you are running this on Ubuntu 20.04+ Server" 10 30
	exit 1;
fi

# Check if running on Ubuntu server and not with a Desktop Enviroment
if [[ $DESKTOP_SESSION != '' ]]; then
	dialog --sleep 2 --infobox "This script is meant for Ubuntu Server not to be run along with a Desktop Enviroment" 10 40
	exit 1;
fi

# Internet check
dialog --sleep 2 --infobox "Checking for internet connection..." 5 40
internet_check=`ping -c 3 retropie.org.uk 2>&1 > /dev/null`
if [[ ! $internet_check -eq 0 ]]; then
	dialog --msgbox "You need to be connected to the internet" 5 40
	exit 1;
fi

dialog --colors --defaultno --yesno "Before we begin, this script will allow the user: \Z1'$default_user'\Zn to run sudo \Z1\Zuwithout\Zn a password \
    for RetroPie to run properly. Are you sure you want to continue?" 10 40

if [[ $? -eq 1 ]]; then
    clear
	exit 0;
fi


################### END PREREQUISITES ##################


# Begin install process
# Update system first
pass=$(tempfile 2>/dev/null)

# If Signal is caught will delete the tempfile that holds the pass
trap "rm -rf $pass" 0 1 2 5 15

dialog --insecure --passwordbox "sudo password" 5 30 2>$pass

if [[ $? -eq 0 && `cat $pass` != '' ]]; then
    cat $pass | sudo -S apt-get update 
else
    exit 1;
fi    

# Check if user is using Nvidia for a GPU or not (might add AMD later if proprietary driver is requested)
DETECT_GPU=$(lspci | grep VGA | awk '{print $5}')

# Install required packages for Nvidia, otherwise it will install required for Intel/AMD
if [[ "$DETECT_GPU" == "NVIDIA" ]]; then
	# get latest Nvidia driver, have to use xserver-xorg one not the nvidia-headless-XXX or it will not properly
	# collect the right resolution and screen size. It will detect, but it will not work properly.
	# Fixed with version 1.1.4+
	LATEST_NVIDIA_DRIVER=$(sudo apt-cache search xserver-xorg-video-nvidia | sort | tail -2 | head -1 | awk '{print $1}')
	sudo apt-get install --no-install-recommends -y $PACKAGES $LATEST_NVIDIA_DRIVER

# Need to filter for amdgpu (only if requested to add proprietary driver) - issue (#2) blocking
elif [[ "$1" == "--non-free" && "$DETECT_GPU" == "AMD" ]]; then
   
   	# Collect latest AMDGPU (non-free) driver
	clear
	printf "\nPlease note that this has not been tested yet, I do not have an AMD GPU to test the free or non-free drivers\n"
	
	read -p "Do you want to install the non-free drivers? (NOT TESTED) y/n: " userInput
	
	if [[ "$userInput" =~ ^(Yes|YES|Y|y|YEs|yES|YEs)$ ]]; then
		LATEST_AMD_DRIVER=$(sudo apt-cache search xserver-xorg-video-amdgpu | sort | tail -1 | awk '{print $1}')
		sudo apt-get install --no-install-recommends -y $PACKAGES $LATEST_AMD_DRIVER
	else
		sudo apt-get install --no-install-recommends -y $PACKAGES
	fi

else
	sudo apt-get install --no-install-recommends -y $PACKAGES
fi

# Error trap for errors in package install
trap 'err $LINENO package_install' ERR
trap - ERR
cd /home/$default_user/
# Get the official RetroPie script

if [[ ! -d "RetroPie-Setup" ]];then
	git clone --depth=1 https://github.com/RetroPie/RetroPie-Setup.git
fi

cd RetroPie-Setup
# Better output to user
# Probably should add tempfile for .out instead of just touching and removing later.
clear
dialog --colors --nocancel --radiolist "You will need to select \Z4'Basic Install'\Zn. Once the \Z4'Basic Install'\Zn is done \Z1\ZuDO NOT REBOOT\Zn Select \Z1'Exit'\Zn" 20 40 2 \
    Y "Yes, I understand" off \
    N "No, get me outta here!" on 2>.out

if [[ $? -eq 0 && `cat .out` == "Y" ]]; then
    sudo ./retropie_setup.sh
    rm .out
else
    rm .out
    exit 1;
fi

while [[ ! "$(ls -A /home/$default_user/RetroPie/roms/)" ]]; do
	dialog --mgsbox "You need to install RetroPie before you can continue." 5 20
	dialog --defaultno --yesno "Would you like to re-run the RetroPie-Setup script?" 10 20
	if [[ $? -eq 1 ]]; then
		sudo ./retropie_setup.sh
	else
        clear
		exit 0;
	fi
done

# Error trap incase retropie setup fails
trap 'err $LINENO retropie_setup_err' ERR
trap - ERR

printf "exec openbox-session" > /home/$default_user/.xinitrc

mkdir -p /home/$default_user/.config/openbox 
# Fix for issue (#5)
printf "xterm -fullscreen -e emulationstation\nxterm -bg black -fg green -fullscreen &\ndisown\nexit" > /home/$default_user/.config/openbox/autostart

# Error trap to make sure file was created
trap 'err emulationstation_autostart_err' ERR
trap - ERR

# Check to see if previously ran
cat $pass | sudo -Sl >/dev/null # used just in case the sudo does not catch in lines below
check_sudo=`sudo grep -Fq "$default_user  ALL=(ALL)   NOPASSWD:ALL" /etc/sudoers`
if [[ ! $check_sudo -eq 0 || $check_sudo != "" || $check_sudo != " " ]]; then

	echo -e "$default_user	ALL=(ALL)	NOPASSWD:ALL" | sudo EDITOR='tee -a' visudo

fi

# Create autologin service for tty1 override
if [[ ! -d "/etc/systemd/system/getty@tty1.service.d/" ]]; then
	sudo mkdir -p "/etc/systemd/system/getty@tty1.service.d/"
fi

if [[ ! -f "/etc/systemd/system/getty@tty1.service.d/override.conf" ]]; then
    cd ../retropie_setup_x86
    echo -e "[Service]\nExecStart=\nExecStart=-/sbin/agetty --autologin $default_user --noclear %I \$TERM\nType=idle" > override.conf
    sudo mv override.conf "/etc/systemd/system/getty@tty1.service.d/"
fi

# Error trap to remove custom service config
trap 'err $LINENO restore_services_err' ERR
trap - ERR

# Had to add `exec startx` instead of startx to run properly. Fixed in issue (#4)
if [[ ! `grep -Fq "pgrep 'tmux|startx' || exec startx -- -nocursor\nsource ~/.bashrc" /home/$default_user/.bash_profile` -eq 0  || ! -f "/home/$default_user/.bash_profile" ]]; then
	printf "pgrep 'tmux|startx' || exec startx -- -nocursor\nsource ~/.bashrc" >> /home/$default_user/.bash_profile
fi


# Add user to audio group
user_groups=$(groups $default_user)
if [[ ! `echo $user_groups | grep -Foq audio` -eq 0 ]]; then
	sudo usermod -aG audio $default_user
fi

# Reset trap to not get triggered
trap - ERR

# Countdown timer
dialog --nook --nocancel --pause "Restarting in..." 8 20 10
dialog --sleep 2 --infobox "Enjoy your RetroPie Setup ;-)" 10 20
sudo reboot
